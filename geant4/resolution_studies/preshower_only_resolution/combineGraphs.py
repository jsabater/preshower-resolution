import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import math
from math import sqrt
import numpy as np
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()


f_3p5 = ROOT.TFile("energy_resolution_plots_3p5W.root","READ")
f_3p5_3X0 = ROOT.TFile("energy_resolution_plots_3p5W_3X0.root","READ")
f_3p5_2X0 = ROOT.TFile("energy_resolution_plots_3p5W_2X0.root","READ")
f_2x_3p5_3X0 = ROOT.TFile("energy_resolution_plots_3p5W_2x_3X0.root","READ")

reso3p5_graph = f_3p5.Get("resolution")
reso3p5_3X0_graph = f_3p5_3X0.Get("resolution")
reso3p5_2X0_graph = f_3p5_2X0.Get("resolution")
reso2x_3p5_graph = f_2x_3p5_3X0.Get("resolution")

print("3p5W")
reso3p5_graph.Print()
print("3p5W_3X0")
reso3p5_3X0_graph.Print()
print("3p5W_2X0")
reso3p5_2X0_graph.Print()
print("3p5W_2x_3X0")
reso2x_3p5_graph.Print()

reso3p5_mean_graph = f_3p5.Get("mean")
reso3p5_3X0_mean_graph = f_3p5_3X0.Get("mean")
reso3p5_2X0_mean_graph = f_3p5_2X0.Get("mean")
reso2x_3p5_mean_graph = f_2x_3p5_3X0.Get("mean")

reso3p5_sigma_graph = f_3p5.Get("sigma")
reso3p5_3X0_sigma_graph = f_3p5_3X0.Get("sigma")
reso3p5_2X0_sigma_graph = f_3p5_2X0.Get("sigma")
reso2x_3p5_sigma_graph = f_2x_3p5_3X0.Get("sigma")

reso3p5_graph.SetMarkerColor(kRed+1)
reso3p5_graph.SetLineColor(kRed+1)
reso3p5_2X0_graph.SetMarkerColor(kAzure+1)
reso3p5_2X0_graph.SetLineColor(kAzure+1)
reso2x_3p5_graph.SetLineColor(kGreen+1)
reso2x_3p5_graph.SetMarkerColor(kGreen+1)

reso3p5_mean_graph.SetMarkerColor(kRed+1)
reso3p5_mean_graph.SetLineColor(kRed+1)
reso3p5_2X0_mean_graph.SetMarkerColor(kAzure+1)
reso3p5_2X0_mean_graph.SetLineColor(kAzure+1)
reso2x_3p5_mean_graph.SetLineColor(kGreen+1)
reso2x_3p5_mean_graph.SetMarkerColor(kGreen+1)

reso3p5_sigma_graph.SetMarkerColor(kRed+1)
reso3p5_sigma_graph.SetLineColor(kRed+1)
reso3p5_2X0_sigma_graph.SetMarkerColor(kAzure+1)
reso3p5_2X0_sigma_graph.SetLineColor(kAzure+1)
reso2x_3p5_sigma_graph.SetLineColor(kGreen+1)
reso2x_3p5_sigma_graph.SetMarkerColor(kGreen+1)

mg = ROOT.TMultiGraph();
mg.Add(reso3p5_graph)
mg.Add(reso3p5_3X0_graph)
mg.Add(reso3p5_2X0_graph)
mg.Add(reso2x_3p5_graph)

mg_mean = ROOT.TMultiGraph();
mg_mean.Add(reso3p5_mean_graph)
mg_mean.Add(reso3p5_3X0_mean_graph)
mg_mean.Add(reso3p5_2X0_mean_graph)
mg_mean.Add(reso2x_3p5_mean_graph)

mg_sigma = ROOT.TMultiGraph();
mg_sigma.Add(reso3p5_sigma_graph)
mg_sigma.Add(reso3p5_3X0_sigma_graph)
mg_sigma.Add(reso3p5_2X0_sigma_graph)
mg_sigma.Add(reso2x_3p5_sigma_graph)

mg.GetXaxis().SetTitle("E_{beam} [GeV]");
mg.GetYaxis().SetTitle("#sigma_{E}/<E>");


mg_mean.GetXaxis().SetTitle("E_{beam} [GeV]");
mg_mean.GetYaxis().SetTitle("Mean");

mg_sigma.GetXaxis().SetTitle("E_{beam} [GeV]");
mg_sigma.GetYaxis().SetTitle("Sigma");

c_reso = ROOT.TCanvas("c_reso", "", 700, 600)
c_reso.cd()
gStyle.SetEndErrorSize(5.) # size of the brackets at the end of the error bars
gPad.SetLogx()
#gPad.SetLogy()


#mg.Draw("apl")
mg.Draw("apl")
#mg.GetXaxis().SetLimits(-100,1000);
#leg =  ROOT.TLegend(0.35,0.7,0.5,0.90);
leg =  ROOT.TLegend(0.45,0.7,0.6,0.90);

leg.SetFillStyle(0);
leg.SetFillColor(0);
leg.SetBorderSize(0);
leg.SetTextFont(42);
leg.SetTextSize(0.035);

leg.AddEntry(reso3p5_graph,"6X0")
leg.AddEntry(reso3p5_3X0_graph,"3X0")
leg.AddEntry(reso3p5_2X0_graph,"2X0")
leg.AddEntry(reso2x_3p5_graph,"2x 3X0")
leg.Draw("same")

gPad.Modified();
#mg.GetHistogram().GetXaxis().SetRangeUser(-10.,3500)
#mg.GetXaxis().SetLimits(0.,2.)
mg.GetXaxis().SetLimits(1e-1,500.)
mg.GetYaxis().SetLimits(0.,0.5)

#mg.SetMinimum(50)

#gPad.Modified()
#gPad.Update()



c_mean = ROOT.TCanvas("c_mean", "", 700, 600)
c_mean.cd()
gPad.SetLogx()

mg_mean.Draw("apl")
leg.Draw("same")


c_sigma = ROOT.TCanvas("c_sigma", "", 700, 600)
c_sigma.cd()
gPad.SetLogx()

mg_sigma.Draw("apl")
leg.Draw("same")


c_reso.SaveAs("resolution_combined.pdf")
c_mean.SaveAs("mean_combined.pdf")
c_sigma.SaveAs("sigma_combined.pdf")
