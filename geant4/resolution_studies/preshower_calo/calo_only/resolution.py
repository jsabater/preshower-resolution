import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import math
from math import sqrt
import numpy as np
import os
from os.path import exists
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()
'''
def DoubleSidedCB2(x, mu, width, a1, p1, a2, p2)
{
   u   = (x-mu)/width
   A1  = TMath::Power(p1/TMath::Abs(a1),p1)*TMath::Exp(-a1*a1/2)
   A2  = TMath::Power(p2/TMath::Abs(a2),p2)*TMath::Exp(-a2*a2/2)
   B1  = p1/TMath::Abs(a1) - TMath::Abs(a1)
   B2  = p2/TMath::Abs(a2) - TMath::Abs(a2)
   
   result(1)
   if:      (u<-a1) result *= A1*TMath::Power(B1-u,-p1)
   elif (u<a2):  result *= TMath::Exp(-u*u/2)
   else:            result *= A2*TMath::Power(B2+u,-p2)
   return result
}

def DoubleSidedCB(x, par)
{
  return(par[0] * DoubleSidedCB2(x[0], par[1],par[2],par[3],par[4],par[5],par[6]))
}
'''
def prepare_graph(graph, name, title, colour = 9, markerStyle = 21, factor = 1):
   # graph settings
   graph.SetTitle(title)
   graph.SetName(name)
   graph.SetMarkerStyle(markerStyle)
   graph.SetMarkerSize(1.4)
   graph.SetMarkerColor(colour)
   graph.SetLineColor(colour)
   # set Y axis
   graph.GetYaxis().SetTitleSize(0.06)
   graph.GetYaxis().SetTitleOffset(1.1)
   graph.GetYaxis().SetLabelSize(0.045)
   graph.GetYaxis().SetNdivisions(504)
   # set X axis
   graph.GetXaxis().SetTitleSize(0.07)
   graph.GetXaxis().SetTitleOffset(1.)
   graph.GetXaxis().SetLabelSize(0.05)
   graph.GetYaxis().SetNdivisions(506)

def prepare_second_graph(secondary, main, name, title, factor = 2):
   # graph settings
   secondary.SetTitle(title)
   secondary.SetName(name)
   secondary.SetMarkerStyle(main.GetMarkerStyle())
   secondary.SetMarkerSize(main.GetMarkerSize())
   secondary.SetMarkerColor(main.GetMarkerColor())
   secondary.SetLineColor(main.GetLineColor())
   # set X axis
   secondary.GetXaxis().SetLabelSize(0)
   secondary.GetXaxis().SetTitleSize(0)
   secondary.GetXaxis().SetTickLength(main.GetXaxis().GetTickLength()*factor)
   # set Y axis
   main.GetYaxis().CenterTitle()
   secondary.GetYaxis().CenterTitle()
   secondary.GetYaxis().SetLabelSize(main.GetYaxis().GetLabelSize()*factor)
   secondary.GetYaxis().SetLabelOffset(main.GetYaxis().GetLabelOffset()/factor)
   secondary.GetYaxis().SetTitleSize(main.GetYaxis().GetTitleSize()*factor)
   secondary.GetYaxis().SetTitleOffset(main.GetYaxis().GetTitleOffset()/factor)
   secondary.GetYaxis().SetTickLength(main.GetYaxis().GetTickLength())
   secondary.GetYaxis().SetNdivisions(506)

gReso = TGraphErrors()
gMean = TGraphErrors()
gSigma = TGraphErrors()
gLinearity = TGraphErrors()



#f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/geant4/simplifiedLayout/500GeV_6layers_1000events.root","READ")

# Longitudinal profile
#files = ["1GeV_faser_mini_1photon_1k.root","10GeV_faser_mini_1photon_1k.root","100GeV_faser_mini_1photon_1k.root","1000GeV_faser_mini_1photon_1k.root","3500GeV_faser_mini_1photon_1k.root"]
#files = ["1GeV_faser_mini_4p6W_1photon_0mm_1k.root","10GeV_faser_mini_4p6W_1photon_0mm_1k.root","100GeV_faser_mini_4p6W_1photon_0mm_1k.root","1000GeV_faser_mini_4p6W_1photon_0mm_1k.root"]
geometry="calo"
#geometry="preshowerUpgrade_calo"
#geometry="preshower_calo"
#geometry="preshower_calo_modified"
#geometry="3p5W_2X0"
#geometry="3p5W_3X0"
#geometry="3p5W_2x_3X0"
#files = ["1000GeV_faser_mini_"+geometry+"_1photon_0mm_1k.root"]
#events=str(10000)
events="100k"
#files = ["10GeV_"+geometry+"_"+events+"events.root","50GeV_"+geometry+"_"+events+"events.root","100GeV_"+geometry+"_"+events+"events.root","250GeV_"+geometry+"_"+events+"events.root","1000GeV_"+geometry+"_1000""events.root"]

files = ["1GeV_"+geometry+"_"+events+"events.root","10GeV_"+geometry+"_"+events+"events.root","50GeV_"+geometry+"_"+events+"events.root","100GeV_"+geometry+"_"+events+"events.root","250GeV_"+geometry+"_"+events+"events.root","500GeV_"+geometry+"_"+events+"events.root","1000GeV_"+geometry+"_"+events+"events.root"]
#files = ["50GeV_"+geometry+"_"+events+"events.root","100GeV_"+geometry+"_"+events+"events.root","250GeV_"+geometry+"_"+events+"events.root","500GeV_"+geometry+"_"+events+"events.root","1000GeV_"+geometry+"_"+events+"events.root"]



for ifile, filename in enumerate(files):
   # take the energy of the photon from the file name
   energy = int(filename.split('GeV')[0])
   print('energy '+str(energy))


   myFile="/eos/project/f/faser-preshower/simulations/geant4/"+geometry+"/"+filename

   # continue if the file does not exist
   file_exists = os.path.exists(myFile)
   if not file_exists:
      print (filename, ' does not exist, skipping')
      continue
   # open the file
   #f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/allpix/noNoiseCenteredASIC/3p5W/"+filename,"READ")
   #f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/allpix/noNoiseCenteredASIC/3p5W_3X0/"+filename,"READ")
   f = ROOT.TFile(myFile,"READ")
   
   eventTree = f.Get("hits")

   # Resolution (charge)
   c_reso = ROOT.TCanvas("c_reso", "", 700, 600)
   c_reso.cd()
   gStyle.SetOptFit(1111)
   #gStyle.SetOptFit(0000)
   #if energy < 250:
      #xmax = energy/1.3
   #xmax = energy*1.5
   xmin = 0.
   xmax = 0.2
   if geometry == "calo":
      xmin = 0.1
      xmax = 0.3
      if energy == 1:
         xmin = 0.1
         xmax = 0.25
      if energy == 10:
         xmin = 1.4
         xmax = 1.9
      if energy == 50:
         xmin = 7.6
         xmax = 8.7
      if energy == 100:
         xmin = 15.5
         xmax = 17.3
      if energy == 250:
         xmin = 39
         xmax = 42.4
      if energy == 500:
         xmin = 74
         xmax = 84
      if energy == 1000:
         xmin = 145
         xmax = 167
         
   if geometry == "preshower_calo":
      xmin = 0.
      xmax = 0.2
      if energy == 1:
         xmin = 0.12
         xmax = 0.25
      if energy == 10:
         xmin = 0.15
         xmax = 0.2
      if energy == 50:
         xmin = 0.15
         xmax = 0.18
      if energy == 100 or energy == 250:
         xmin = 0.165
         xmax = 0.175
      if energy == 500:
         xmin = 0.165
         xmax = 0.175
      if energy == 1000 or energy == 3500:
         xmin = 0.165
         xmax = 0.175
      #if energy == 100:
      #   xmin = 0.15
      #   xmax = 0.2
   
      
   if geometry == "preshowerUpgrade_calo":
      if energy == 1:
         xmin = 0
         xmax = 0.14
      if energy == 10:
         xmin = 0.
         xmax = 1.8
      if energy == 50:
         xmin = 1
         xmax = 9
      if energy == 100:
         xmin = 8
         xmax = 18
      if energy == 250:
         xmin = 26
         xmax = 42
      if energy == 500:
         xmin = 60
         xmax = 85
      if energy == 1000:
         xmin = 130
         xmax = 170


   if geometry == "preshower_calo_modified":
      if energy == 100:
         xmin = 0.1
         xmax = 0.18
      if energy == 500:
         xmin = 0.135
         xmax = 0.175
      if energy == 1000:
         xmin = 0.14
         xmax = 0.175
   
   bins = 100
   #bins = 50
   #bins = 30
   if energy == 1:
      bins = 100
#   else:
#      bins = 50

   h_reso = ROOT.TH1F("h_reso", "h_reso", bins, xmin, xmax)
   #   eventTree.Draw("(Edep_Si+Edep_Sci)/"+str(energy)+" >> h_reso","")
   eventTree.Draw("(Edep_Sci_calo) >> h_reso","")
   
   # crystal ball function
   preFitReso = TF1("leak", "crystalball",xmin,xmax)
   # gaussian 
   #preFitReso_gaus = TF1("gausFit", "gaus",0,xmax)
   gaus_xmin = xmin
   gaus_xmax = xmax

   preFitReso_gaus = TF1("preGaus", "gaus",gaus_xmin,gaus_xmax)
   #preFitReso_gaus = TF1("preGaus", "gaus",20,40)
   resultPre_gaus = h_reso.Fit(preFitReso_gaus, "SQRN")
   finalGaus_xmin = resultPre_gaus.Get().Parameter(1) - 2. * resultPre_gaus.Get().Parameter(2)
   finalGaus_xmax = resultPre_gaus.Get().Parameter(1) + 2. * resultPre_gaus.Get().Parameter(2)
   if (geometry == "3p5W_2x_3X0_2Si"):
      finalGaus_xmin = 5
      finalGaus_xmax = 25
   fitReso_gaus = TF1("finalGaus", "gaus", finalGaus_xmin, finalGaus_xmax)
   result_gaus = h_reso.Fit(fitReso_gaus, "SQR")


   mean = result_gaus.Get().Parameter(1)
   dmean = result_gaus.Get().Error(1)
   sigma = result_gaus.Get().Parameter(2)
   dsigma = result_gaus.Get().Error(2)
   resolution = sigma/mean
   resolutionErrorSigma = dsigma / mean
   resolutionErrorMean = dmean * sigma / ( mean ** 2)
   resolutionError = sqrt( resolutionErrorSigma ** 2 +  resolutionErrorMean ** 2 )


   cbPreConstant = 50 # controls the height of the distribution
   cbPreMean = mean
   cbPreSigma = sigma
   cbPreAlpha = 0.1 # controls the slope from x=0 to x = xMean
   cbPreN = 45 # controls the value of y at x = 0, the lower N is, the higher the y value
   if geometry == "calo":
      if energy == 1:
         cbPreConstant = 4000
         cbPreAlpha = 1
         cbPreN = 1
      if energy == 10:
         cbPreConstant = 5000
         cbPreAlpha = 1
         cbPreN = 1
      if energy == 50:
         cbPreConstant = 6000
         cbPreAlpha = 1
         cbPreN = 1
      if energy == 100:
         cbPreConstant = 6000
         cbPreAlpha = 1
         cbPreN = 1
      if energy >= 250:
         cbPreConstant = 6500
         cbPreMean = mean
         cbPreSigma = sigma
         cbPreAlpha = 0.6
         cbPreN = 1
         cbPreConstant = 150
      if energy > 250:
         cbPreConstant = 8000
         cbPreMean = mean
         cbPreSigma = sigma
         cbPreAlpha = 0.6
         cbPreN = 1
         cbPreConstant = 150
      
   if geometry == "preshowerUpgrade_calo":
      if energy == 1:
         cbPreMean = mean
         cbPreSigma = sigma
         cbPreConstant = 150 # controls the height of the distribution
         cbPreAlpha = -1
         cbPreN = 1
      if energy == 10:
         cbPreConstant = 420
         cbPreAlpha = -1
         cbPreN = 1
      if energy < 100 and energy > 10:
         #cbPreConstant = 50 # controls the height of the distribution
         #cbPreConstant = 250 # controls the height of the distribution
         cbPreConstant = 150 # controls the height of the distribution
         cbPreMean = mean
         cbPreSigma = sigma
         cbPreAlpha = 0.6 # controls the slope from x=0 to x = xMean
         #cbPreN = 45 # controls the value of y at x = 0, the lower N is, the higher the y value
         cbPreN = 0.5
      if energy == 50:
         cbPreConstant = 300
         cbPreAlpha = 1
         cbPreN = 1
      if energy == 100:
         cbPreConstant = 550
         cbPreAlpha = 1
         cbPreN = 1


   # crystal ball function fit
   preFitReso.SetParameters(cbPreConstant, cbPreMean, cbPreSigma, cbPreAlpha, cbPreN)
   preFitReso.SetLineColor(kAzure)
   #resultPreFitReso = h_reso.Fit(preFitReso, "SQRN")
   #resultPreFitReso = h_reso.Fit(preFitReso, "SQR")
   resultPreFitReso = h_reso.Fit(preFitReso, "SREM")
   cbMean = resultPreFitReso.Get().Parameter(1)
   cbSigma = resultPreFitReso.Get().Parameter(2)
   cbdMean = resultPreFitReso.Get().Error(1)
   cbdSigma = resultPreFitReso.Get().Error(2)
   cbResolutionErrorMean = cbdMean * cbSigma / (cbMean**2)
   cbResolutionErrorSigma = cbdSigma/cbSigma
   cbResolution = cbSigma/cbMean
   cbResolutionError = sqrt(cbResolutionErrorSigma ** 2 + cbResolutionErrorMean ** 2)
   print('mean ',cbMean)
   print('sigma ',cbSigma)
   print('Filling ',cbResolution)
   print('Filling error',cbResolutionError)      
   
   #if energy == 1:
      #fitReso_gaus.SetLineColor(kAzure)
   fitReso_gaus.Draw("same")
   #else:
      #preFitReso.SetLineColor(kAzure)
    #  preFitReso.Draw("same")

   print('starting 1st integral')
   #cbIntHalf = preFitReso.Integral()
   lowercbInt = preFitReso.Integral(0,cbMean)
   # lets integrate from the mean to 2*mean (just random value I chose that should cover most of the distribution)
   uppercbInt = preFitReso.Integral(cbMean,cbMean*2)
   # calculate 68% interval of the distribution on each side
   npLow=np.linspace(0,cbMean,100)
   # lower part of the distribution
   for i in npLow:
      cbInt_tmp = preFitReso.Integral(i,cbMean)
      if (lowercbInt-cbInt_tmp > 0.32*lowercbInt): # 68% of the integral on one side (or 32%, depending to which side you do the integral)
         sigmaLow = i
         break
   # upper part of the distribution
   print('starting 2nd integral')
   npHigh=np.linspace(cbMean,cbMean*2,100)
   for i in npHigh:
      cbInt_tmp = preFitReso.Integral(cbMean,i)
      if (cbInt_tmp > 0.68*uppercbInt): # 68% of the integral on one side (or 32%, depending to which side you do the integral)
         sigmaHigh = i
         break
   
   print('sigma low = ', cbMean-sigmaLow)
   print('sigma high = ', sigmaHigh-cbMean)
   mySigma=(sigmaHigh-sigmaLow)/2.
   myResolution=mySigma/cbMean
   print('final sigma = ', mySigma)
   print('Resolution = ', myResolution)
   line_low = ROOT.TLine(sigmaLow,0,sigmaLow,200)
   line_low.SetLineStyle(2)
#   line_low.Draw("same");

   line_high = ROOT.TLine(sigmaHigh,0,sigmaHigh,200)
   line_high.SetLineStyle(2)
#   line_high.Draw("same");


   
   # resolution taking integrals
   #gReso.SetPoint(ifile, energy, myResolution)
   # resolution from crystal ball sigma
   gReso.SetPoint(ifile, energy, cbResolution)
   gReso.SetPointError(ifile, 0, cbResolutionError)
   #gReso.SetPointError(ifile, 0, 0)
   # Mean graph
   gMean.SetPoint(ifile, energy, cbMean)
   #gMean.SetPointError(ifile, 0, cbdMean)
   gMean.SetPointError(ifile, 0, 0)
   # Sigma graph
   gSigma.SetPoint(ifile, energy, cbSigma)
   gSigma.SetPointError(ifile, 0, cbdSigma)

   # linearity
   fcToGeV = 6250.*3.6/(1.e9)
   #linearity = ( cbMean*fcToGeV-energy ) / energy
   Fs = 1.74993e-03
   Eleakage = 0.34553253
   Eunknown = 0.65715240;
   # since we are plotting QfCtot/Ebeam, we have to multiply times Ebeam (energy)
   #Ereco = energy*cbMean*fcToGeV/(Fs*Eleakage)
   #Ereco = energy*cbMean*fcToGeV/(Fs*Eleakage*Eunknown)
   Ereco = energy*cbMean*fcToGeV
   print('Ereco = ',Ereco)
   #linearity = abs( Ereco - energy) / energy
   linearity = Ereco / energy
   print('linearity = ',linearity)
   linearityError = cbdMean*fcToGeV*Eleakage / (Fs*energy)
   gLinearity.SetPoint(ifile, energy, linearity)
   #gLinearity.SetPointError(ifile, 0, linearityError)
   #gReso.SetPoint(ifile, 1./sqrt(energy), myResolution)
   #gReso.SetPointError(ifile, 0, cbResolutionError)

   #factor=2 # meaning the upper plot is twice smaller than the bottom plot
   #prepare_second_graph(gLinearity, gReso, "linearity", ";E_{beam} [GeV];(#LTE_{rec}#GT-E_{beam})/E_{beam}", factor)
   c_reso.SaveAs("plots/h_reso_"+geometry+"_"+str(energy)+".pdf")
   c_reso.Close()

c_energyReso = ROOT.TCanvas("c_energyReso", "", 700, 600)
c_energyReso.cd()
gPad.SetLogx()
#gPad.SetLogy()
colour = kRed + 1
#colour = kAzure + 1
prepare_graph(gReso, "resolution", ";E_{beam} (GeV);#sigma_{E}/<E>", colour, 21)
#fRes = TF1("res", "sqrt([0]*[0] + pow([1]/sqrt(x),2))",5,600)
fRes = TF1("res", "[0]/sqrt(x)+[1]",0.1,3500)
#fRes = TF1("res", "[0]*sqrt(x)",0.1,3500)
fRes.SetParName(0,"const")
#fRes.SetParName(1,"sqrt")
fRes.SetLineColor(colour)
# fit 1/sqrt(x)
fitResult = gReso.Fit(fRes, 'SQRN')
print('Reso a',fitResult.Get().Parameter(0))
print('Reso b',fitResult.Get().Parameter(1))


# Draw graph and all labels
gReso.Draw("ape")


c_energyReso.SaveAs("plots/h_reso_"+geometry+".pdf")
c_energyReso.Close()




prepare_graph(gMean, "mean", ";E_{beam} (GeV);Mean", colour, 21)
prepare_graph(gSigma, "sigma", ";E_{beam} (GeV);Sigma", colour, 21)

c_linearity = ROOT.TCanvas("c_linearity", "", 700, 600)
c_linearity.cd()
gPad.SetLogx()
colour = kAzure + 1
prepare_graph(gLinearity, "linearity", ";E_{beam} [GeV];<E>/E_{beam}", colour, 21)
#gLinearity.GetYaxis().SetRangeUser(0.9,1.1)
#
#gLinearity.GetYaxis().SetRangeUser(0.5,1.1)
gLinearity.GetYaxis().SetRangeUser(0.,0.001)
gLinearity.Draw("ape")
c_linearity.SaveAs("plots/h_linearity.pdf")
c_linearity.Close()


plots = TFile("energy_resolution_plots_"+geometry+".root","RECREATE")
gReso.Write()
gMean.Write()
gSigma.Write()
gLinearity.Write()
plots.Write()
plots.Close()
