import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import math
from math import sqrt
import numpy as np
import ctypes
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/Users/jordi/code/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/Users/jordi/code/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()

f_0fC = ROOT.TFile("slope_0fC.root","READ")
f_0p5fC = ROOT.TFile("slope_0.5fC.root","READ")
f_1fC = ROOT.TFile("slope_1fC.root","READ")
f_2fC = ROOT.TFile("slope_2fC.root","READ")

graph_0fC = f_0fC.Get("slope")
graph_0p5fC = f_0p5fC.Get("slope")
graph_1fC = f_1fC.Get("slope")
graph_2fC = f_2fC.Get("slope")


graph_0fC.RemovePoint(0) # removing E=1 GeV
graph_0fC.RemovePoint(0) # removing E=10 GeV
#graph_0fC.RemovePoint(0) # removing E=50 GeV

graph_0p5fC.RemovePoint(0) # removing E=1 GeV
graph_0p5fC.RemovePoint(0) # removing E=10 GeV
#graph_0p5fC.RemovePoint(0) # removing E=50 GeV

graph_1fC.RemovePoint(0) # removing E=1 GeV
graph_1fC.RemovePoint(0) # removing E=10 GeV
#graph_1fC.RemovePoint(0) # removing E=50 GeV

graph_2fC.RemovePoint(0) # removing E=1 GeV
graph_2fC.RemovePoint(0) # removing E=10 GeV
#graph_2fC.RemovePoint(0) # removing E=50 GeV

def prepare_graph(graph,color):
    graph.SetMarkerColor(color)
    graph.SetLineColor(color)
    graph.SetLineWidth(2)

prepare_graph(graph_0fC,kRed+1)
prepare_graph(graph_0p5fC,kBlue+1)
prepare_graph(graph_1fC,kGreen-6)
prepare_graph(graph_2fC,kMagenta)


mg = ROOT.TMultiGraph()
mg.Add(graph_0fC)
mg.Add(graph_0p5fC)
mg.Add(graph_1fC)
mg.Add(graph_2fC)

mg.GetXaxis().SetTitle("E_{beam} [GeV]")
mg.GetYaxis().SetTitle("Slope")


c_slope = ROOT.TCanvas("c_slope", "", 700, 600)
c_slope.cd()
mg.Draw("apl")

leg =  ROOT.TLegend(0.3,0.4,0.5,0.65)
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.035)

leg.AddEntry(graph_0fC,"0fC","pe")
leg.AddEntry(graph_0p5fC,"0.5fC","pe")
leg.AddEntry(graph_1fC,"1fC","pe")
leg.AddEntry(graph_2fC,"2fC","pe")
leg.Draw()


c_slope.SaveAs("plots/slope_combined.pdf")

c_slope.Close()

# get the mean of each graph and plot the deviation of each point wrt to this mean
mean_0fC = graph_0fC.GetMean(2)
mean_0p5fC = graph_0p5fC.GetMean(2)
mean_1fC = graph_1fC.GetMean(2)
mean_2fC = graph_2fC.GetMean(2)
print('Mean 0fC : ',mean_0fC)
print('Mean 0.5fC : ',mean_0p5fC)
print('Mean 1fC : ',mean_1fC)

graph_arr = [graph_0fC,graph_0p5fC,graph_1fC,graph_2fC]


graph_err_0fC = TGraphErrors() 
graph_err_0p5fC = TGraphErrors()
graph_err_1fC = TGraphErrors()
graph_err_2fC = TGraphErrors()

prepare_graph(graph_err_0fC,kRed+1)
prepare_graph(graph_err_0p5fC,kBlue+1)
prepare_graph(graph_err_1fC,kGreen-6)
prepare_graph(graph_err_2fC,kMagenta)

graph_err_arr = [graph_err_0fC,graph_err_0p5fC,graph_err_1fC,graph_err_2fC]
mg_2 = ROOT.TMultiGraph()
# Subtract the value from each point
for count,igraph in enumerate(graph_arr):
    for i in range(graph_arr[count].GetN()):
        #x, y = ROOT.Double(0), ROOT.Double(0)
        x = ctypes.c_double() 
        y = ctypes.c_double() 
        igraph.GetPoint(i, x, y)
        mean = igraph.GetMean(2)
        graph_err_arr[count].SetPoint(i, x.value, (y.value - mean)/mean)
    # add every graph to the multigraph
    mg_2.Add(graph_err_arr[count])



c = ROOT.TCanvas("c_test", "", 700, 600)
c.cd()
mg_2.Draw("apl")

mg_2.GetXaxis().SetTitle("E_{beam} [GeV]")
mg_2.GetYaxis().SetTitle("Rel. Difference")

leg2 =  ROOT.TLegend(0.7,0.72,0.9,0.90)
leg2.SetFillStyle(0)
leg2.SetFillColor(0)
leg2.SetBorderSize(0)
leg2.SetTextFont(42)
leg2.SetTextSize(0.035)

leg2.AddEntry(graph_err_0fC,"0fC","pe")
leg2.AddEntry(graph_err_0p5fC,"0.5fC","pe")
leg2.AddEntry(graph_err_1fC,"1fC","pe")
leg2.AddEntry(graph_err_2fC,"2fC","pe")
leg2.Draw()


c.SaveAs("plots/slope_mean_relError.pdf")


# calculate the relative uncertainty on the total energy
f_0fC = ROOT.TFile("th2_0fC.root","READ")
f_0p5fC = ROOT.TFile("th2_0.5fC.root","READ")
f_1fC = ROOT.TFile("th2_1fC.root","READ")
f_2fC = ROOT.TFile("th2_2fC.root","READ")

# take the profiles
prof_50GeV = f_0fC.Get("hist_2d_50GeV_pfx")
prof_100GeV = f_0fC.Get("hist_2d_100GeV_pfx")
prof_250GeV = f_0fC.Get("hist_2d_250GeV_pfx")
prof_500GeV = f_0fC.Get("hist_2d_500GeV_pfx")
prof_1000GeV = f_0fC.Get("hist_2d_1000GeV_pfx")

