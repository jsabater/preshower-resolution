import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import math
from math import sqrt
import numpy as np
import os
from os.path import exists
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()


def prepare_graph(graph, name, title, colour = 9, markerStyle = 21, factor = 1):
   # graph settings
   graph.SetTitle(title)
   graph.SetName(name)
   graph.SetMarkerStyle(markerStyle)
   graph.SetMarkerSize(1.4)
   graph.SetMarkerColor(colour)
   graph.SetLineColor(colour)
   # set Y axis
   graph.GetYaxis().SetTitleSize(0.06)
   graph.GetYaxis().SetTitleOffset(1.1)
   graph.GetYaxis().SetLabelSize(0.045)
   graph.GetYaxis().SetNdivisions(504)
   # set X axis
   graph.GetXaxis().SetTitleSize(0.07)
   graph.GetXaxis().SetTitleOffset(1.)
   graph.GetXaxis().SetLabelSize(0.05)
   graph.GetYaxis().SetNdivisions(506)

def prepare_second_graph(secondary, main, name, title, factor = 2):
   # graph settings
   secondary.SetTitle(title)
   secondary.SetName(name)
   secondary.SetMarkerStyle(main.GetMarkerStyle())
   secondary.SetMarkerSize(main.GetMarkerSize())
   secondary.SetMarkerColor(main.GetMarkerColor())
   secondary.SetLineColor(main.GetLineColor())
   # set X axis
   secondary.GetXaxis().SetLabelSize(0)
   secondary.GetXaxis().SetTitleSize(0)
   secondary.GetXaxis().SetTickLength(main.GetXaxis().GetTickLength()*factor)
   # set Y axis
   main.GetYaxis().CenterTitle()
   secondary.GetYaxis().CenterTitle()
   secondary.GetYaxis().SetLabelSize(main.GetYaxis().GetLabelSize()*factor)
   secondary.GetYaxis().SetLabelOffset(main.GetYaxis().GetLabelOffset()/factor)
   secondary.GetYaxis().SetTitleSize(main.GetYaxis().GetTitleSize()*factor)
   secondary.GetYaxis().SetTitleOffset(main.GetYaxis().GetTitleOffset()/factor)
   secondary.GetYaxis().SetTickLength(main.GetYaxis().GetTickLength())
   secondary.GetYaxis().SetNdivisions(506)

def prepare_th2(th2,titlex, titley, titlez):
   #th2.SetTitleOffset(1.0)
   th2.GetXaxis().SetTitleOffset(1.3)
   #th2.GetZaxis().SetLabelSize(0.03);
   th2.GetZaxis().SetLabelSize(0.04);
   th2.GetXaxis().SetLabelSize(0.04);
   th2.GetYaxis().SetLabelSize(0.04);
   th2.GetXaxis().SetTitle(titlex);
   th2.GetYaxis().SetTitle(titley);
   th2.GetZaxis().SetTitle(titlez);



gReso = TGraphErrors()

#geometry="preshower_calo"
geometry="preshowerUpgrade_calo"
events=str(10000)
files = ["1GeV_"+geometry+"_"+events+"events.root","10GeV_"+geometry+"_"+events+"events.root","50GeV_"+geometry+"_"+events+"events.root","100GeV_"+geometry+"_"+events+"events.root","250GeV_"+geometry+"_"+events+"events.root","500GeV_"+geometry+"_"+events+"events.root","1000GeV_"+geometry+"_"+events+"events.root"]
#files = ["50GeV_"+geometry+"_"+events+"events.root","100GeV_"+geometry+"_"+events+"events.root","250GeV_"+geometry+"_"+events+"events.root","500GeV_"+geometry+"_"+events+"events.root","1000GeV_"+geometry+"_"+events+"events.root"]



for ifile, filename in enumerate(files):
   # take the energy of the photon from the file name
   energy = int(filename.split('GeV')[0])
   print('energy '+str(energy))


   myFile="/eos/project/f/faser-preshower/simulations/geant4/"+geometry+"/"+filename

   # continue if the file does not exist
   file_exists = os.path.exists(myFile)
   if not file_exists:
      print (filename, ' does not exist, skipping')
      continue
   # open the file
   #f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/allpix/noNoiseCenteredASIC/3p5W/"+filename,"READ")
   #f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/allpix/noNoiseCenteredASIC/3p5W_3X0/"+filename,"READ")
   f = ROOT.TFile(myFile,"READ")

   eventTree = f.Get("hits")

   # Resolution (charge)
   c_reso = ROOT.TCanvas("c_reso", "", 700, 600)
   c_reso.cd()
   gStyle.SetOptFit(1111)


   xmin = 0
   xmax = 10
   ymin = 0
   ymax = 200
   bins_x = 100
   bins_y = 100

   if energy == 1:
      xmax = 0.15
      ymax = 0.3
   if energy == 10:
      xmax = 0.4
      ymax = 2
   if energy == 50:
      ymin = 7
      xmax = 1
      ymax = 10
   if energy == 100:
      ymin = 14
      xmax = 1.2
      ymax = 18
   if energy == 250:
      ymin = 35
      xmax = 1.6
      ymax = 45
   if energy == 500:
      ymin = 70
      xmax = 1.4
      ymax = 90
   if energy == 1000:
      ymin = 160
      xmax = 1.7
      ymax = 180
      
   if geometry == "preshowerUpgrade_calo":
      if energy == 1:
         xmax = 0.003
      if energy == 10:
         xmax = 0.014
      if energy == 50:
         xmax = 0.05
      if energy == 100:
         xmax = 0.06
      if energy == 250:
         xmax = 0.14
      if energy == 500:
         xmax = 0.18
      if energy == 1000:
         xmax = 0.3

   ymin = ymin / 10;

   h_reso = ROOT.TH2F("h_reso", "h_reso", bins_x, xmin, xmax, bins_y, ymin, ymax)
   variable = "Edep_Sci_calo:Edep_Sci"
   if geometry == "preshowerUpgrade_calo":
      variable = "Edep_Sci_calo:Edep_Si"
   eventTree.Draw(variable+" >> h_reso","","COLZ")
   
   prof = h_reso.ProfileX()
   
   fitReso_pol = TF1("finalPol", "pol1", xmin, xmax)
   result_pol = prof.Fit(fitReso_pol, "SRN")
   gPad.SetRightMargin(0.17)
   prepare_th2(h_reso, "E_{Si} [GeV]", "E_{calo} [GeV]", "Events")
   h_reso.Draw("COLZ")
   prof.Draw("same")
   fitReso_pol.Draw("same")
   c_reso.SaveAs("plots/th2_"+str(energy)+".pdf")

   
   const = result_pol.Get().Parameter(0)
   dconst = result_pol.Get().Error(0)
   slope = result_pol.Get().Parameter(1)
   slopeError = result_pol.Get().Error(1)
   
   gReso.SetPoint(ifile, energy, slope)
   gReso.SetPointError(ifile, 0, slopeError)
   

c_slope = ROOT.TCanvas("c_slope", "", 700, 600)
c_slope.cd()
prepare_graph(gReso, "slope", ";E_{beam} [GeV];Slope", kBlack, 21)
gReso.Draw("APE")
c_slope.SaveAs("plots/slope.pdf")
