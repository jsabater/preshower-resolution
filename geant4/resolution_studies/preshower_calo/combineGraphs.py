import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import math
from math import sqrt
import numpy as np
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()


f_3p5 = ROOT.TFile("preshower_calo_correction/chargeThreshold/energy_resolution_plots_preshowerUpgrade_calo_1fC.root","READ")
#f_3p5 = ROOT.TFile("energy_resolution_plots_preshowerUpgrade_calo_0.5fC.root","READ")

#f_3p5_upgrade = ROOT.TFile("energy_resolution_plots_preshower_calo_modified.root","READ")
f_3p5_upgrade = ROOT.TFile("calo_only/energy_resolution_plots_calo.root","READ")
f_3p5_2X0 = ROOT.TFile("no_preshower_correction/energy_resolution_plots_preshowerUpgrade_calo.root","READ")
f_2x_3p5_3X0 = ROOT.TFile("preshower_calo_correction/noChargeThreshold/energy_resolution_plots_preshowerUpgrade_calo.root","READ") 


reso3p5_graph = f_3p5.Get("resolution")
reso3p5_upgrade_graph = f_3p5_upgrade.Get("resolution")
reso3p5_2X0_graph = f_3p5_2X0.Get("resolution")
reso2x_3p5_graph = f_2x_3p5_3X0.Get("resolution")




print("3p5W")
reso3p5_graph.Print()
print("3p5W_upgrade")
reso3p5_upgrade_graph.Print()
print("3p5W_2X0")
reso3p5_2X0_graph.Print()
print("3p5W_2x_3X0")
reso2x_3p5_graph.Print()

reso3p5_mean_graph = f_3p5.Get("mean")
reso3p5_upgrade_mean_graph = f_3p5_upgrade.Get("mean")
reso3p5_2X0_mean_graph = f_3p5_2X0.Get("mean")
reso2x_3p5_mean_graph = f_2x_3p5_3X0.Get("mean")

reso3p5_sigma_graph = f_3p5.Get("sigma")
reso3p5_upgrade_sigma_graph = f_3p5_upgrade.Get("sigma")
reso3p5_2X0_sigma_graph = f_3p5_2X0.Get("sigma")
reso2x_3p5_sigma_graph = f_2x_3p5_3X0.Get("sigma")

reso3p5_graph.SetMarkerColor(kRed+1)
reso3p5_graph.SetLineColor(kRed+1)
reso3p5_graph.SetMarkerSize(0.8)
reso3p5_2X0_graph.SetMarkerColor(kAzure+1)
reso3p5_2X0_graph.SetLineColor(kAzure+1)
reso3p5_2X0_graph.SetMarkerStyle(21)
reso3p5_2X0_graph.SetMarkerSize(0.8)

reso2x_3p5_graph.SetLineColor(kGreen+1)
reso2x_3p5_graph.SetMarkerColor(kGreen+1)
reso2x_3p5_graph.SetMarkerSize(0.8)

reso3p5_mean_graph.SetMarkerColor(kRed+1)
reso3p5_mean_graph.SetLineColor(kRed+1)
reso3p5_2X0_mean_graph.SetMarkerColor(kAzure+1)
reso3p5_2X0_mean_graph.SetLineColor(kAzure+1)
reso2x_3p5_mean_graph.SetLineColor(kGreen+1)
reso2x_3p5_mean_graph.SetMarkerColor(kGreen+1)

reso3p5_sigma_graph.SetMarkerColor(kRed+1)
reso3p5_sigma_graph.SetLineColor(kRed+1)
reso3p5_2X0_sigma_graph.SetMarkerColor(kAzure+1)
reso3p5_2X0_sigma_graph.SetLineColor(kAzure+1)
reso2x_3p5_sigma_graph.SetLineColor(kGreen+1)
reso2x_3p5_sigma_graph.SetMarkerColor(kGreen+1)

reso3p5_upgrade_graph.SetMarkerStyle(22)

mg = ROOT.TMultiGraph();
mg.Add(reso3p5_graph)
mg.Add(reso3p5_upgrade_graph)
mg.Add(reso3p5_2X0_graph)
mg.Add(reso2x_3p5_graph)

mg_mean = ROOT.TMultiGraph();
mg_mean.Add(reso3p5_mean_graph)
mg_mean.Add(reso3p5_upgrade_mean_graph)
mg_mean.Add(reso3p5_2X0_mean_graph)
mg_mean.Add(reso2x_3p5_mean_graph)

mg_sigma = ROOT.TMultiGraph();
mg_sigma.Add(reso3p5_sigma_graph)
#mg_sigma.Add(reso3p5_upgrade_sigma_graph)
mg_sigma.Add(reso3p5_2X0_sigma_graph)
mg_sigma.Add(reso2x_3p5_sigma_graph)

mg.GetXaxis().SetTitle("E_{beam} [GeV]");
mg.GetYaxis().SetTitle("#sigma_{E}/<E>");


mg_mean.GetXaxis().SetTitle("E_{beam} [GeV]");
mg_mean.GetYaxis().SetTitle("Mean");

mg_sigma.GetXaxis().SetTitle("E_{beam} [GeV]");
mg_sigma.GetYaxis().SetTitle("Sigma");

c_reso = ROOT.TCanvas("c_reso", "", 700, 600)
c_reso.cd()
gStyle.SetOptFit(0000)
gStyle.SetEndErrorSize(5.) # size of the brackets at the end of the error bars
gPad.SetLogx()
#gPad.SetLogy()


#mg.Draw("apl")
mg.Draw("ap")
#mg.GetXaxis().SetLimits(-100,1000);
leg =  ROOT.TLegend(0.45,0.7,0.6,0.90)

'''
# Fit the resolution
colour=kGreen+1
fRes = TF1("res", "[0]/sqrt(x)+[1]",1,3500)
fRes.SetParName(0,"sqrt")
fRes.SetParName(1,"const")
fRes.SetLineColor(colour)
fitResult = reso2x_3p5_graph.Fit(fRes, 'SQR')
sqrtTerm=fitResult.Get().Parameter(0)
constTerm=fitResult.Get().Parameter(1)
colour=kBlack
fRes.SetLineColor(colour)
fitResult = reso3p5_upgrade_graph.Fit(fRes, 'SQR')
sqrtTerm_caloOnly=fitResult.Get().Parameter(0)
constTerm_caloOnly=fitResult.Get().Parameter(1)
'''
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.035)

#reso3p5_2X0_graph.RemovePoint(4)
#reso2x_3p5_graph.RemovePoint(4)
#leg.AddEntry(reso3p5_2X0_graph,"Upgraded preshower + Calorimeter","ape")
leg.AddEntry(reso3p5_2X0_graph,"6X_{0} + Calorimeter","ape")
#leg.AddEntry(reso2x_3p5_graph,"Corrected C+P, "+str(round(sqrtTerm*100,2))+"%/#sqrt{E} + "+str(round(constTerm*100,2))+"%")
leg.AddEntry(reso2x_3p5_graph,"Preshower + Calorimeter","ape")
leg.AddEntry(reso3p5_graph,"Preshower (Q > 1fC) + Calorimeter","ape")
#leg.AddEntry(reso3p5_upgrade_graph,"Calo only, "+str(round(sqrtTerm_caloOnly*100,2))+"%/#sqrt{E} + "+str(round(constTerm_caloOnly*100,2))+"%")
leg.AddEntry(reso3p5_upgrade_graph,"Calorimeter","ape")
leg.Draw("same")

gPad.Modified();
#mg.GetHistogram().GetXaxis().SetRangeUser(-10.,3500)
#mg.GetXaxis().SetLimits(0.,2.)


#mg.GetXaxis().SetLimits(1e-1,1500.)
#mg.GetYaxis().SetLimits(0.,0.5)
# to set axis ranges use this
mg.GetXaxis().SetLimits(30,1500.)
#mg.GetXaxis().SetLimits(0.7,1500.)
mg.SetMinimum(-0.04)
mg.SetMaximum(0.2)
#mg.SetMaximum(0.4)

#mg.SetMinimum(50)

#gPad.Modified()
#gPad.Update()



c_mean = ROOT.TCanvas("c_mean", "", 700, 600)
c_mean.cd()
gPad.SetLogx()

mg_mean.Draw("apl")
leg.Draw("same")


c_sigma = ROOT.TCanvas("c_sigma", "", 700, 600)
c_sigma.cd()
gPad.SetLogx()

mg_sigma.Draw("apl")
leg.Draw("same")


c_reso.SaveAs("resolution_combined.pdf")
c_mean.SaveAs("mean_combined.pdf")
c_sigma.SaveAs("sigma_combined.pdf")
