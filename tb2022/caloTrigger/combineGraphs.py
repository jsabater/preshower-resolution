import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import math
from math import sqrt
import numpy as np
import ctypes
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()


f_data = ROOT.TFile("data/energy_resolution_plots_preshower_calo.root","READ")
f_mc = ROOT.TFile("mc/energy_resolution_plots_latest_281022_FTFP_BERT.root","READ")
#f_mc_0p3X0_10m = ROOT.TFile("mc/energy_resolution_plots_0p3X0_10m.root","READ")
#f_mc_0p3X0_10m = ROOT.TFile("mc/energy_resolution_plots_0p2X0_10m.root","READ")
#f_mc_0p3X0_10m = ROOT.TFile("mc/energy_resolution_plots_0p3X0_20m.root","READ")
#f_mc_0p3X0_10m = ROOT.TFile("mc/energy_resolution_plots_301022_FTFP_BERT_EMM.root","READ")

#f_mc_0p3X0_10m = ROOT.TFile("mc/energy_resolution_plots_0p2X0_50m.root","READ")
f_mc_0p3X0_10m = ROOT.TFile("mc_h2beamline/energy_resolution_plots_H2_beamline.root","READ")
#f_mc_0p3X0_50m = ROOT.TFile("mc/energy_resolution_plots_0p2X0_10m.root","READ")
f_mc_0p3X0_50m = ROOT.TFile("mc_h2beamline/energy_resolution_plots_H2_beamline.root","READ")
#f_mc_0p3X0_50m = ROOT.TFile("mc/energy_resolution_plots_0p15X0_75m.root","READ")
data_graph = f_data.Get("resolution")
mc_graph = f_mc.Get("resolution")
mc_0p3X0_10m_graph = f_mc_0p3X0_10m.Get("resolution")
mc_0p3X0_50m_graph = f_mc_0p3X0_50m.Get("resolution")

print('data')
data_graph.Print()
print('simulation')
mc_graph.Print()


mc_graph.SetMarkerColor(kRed+1)
mc_graph.SetLineColor(kRed+1)
mc_graph.SetMarkerStyle(22)

mc_0p3X0_10m_graph.SetMarkerColor(kGreen+1)
mc_0p3X0_10m_graph.SetLineColor(kGreen+1)
mc_0p3X0_10m_graph.SetMarkerStyle(22)

mc_0p3X0_50m_graph.SetMarkerColor(kAzure-6)
mc_0p3X0_50m_graph.SetLineColor(kAzure-6)
mc_0p3X0_50m_graph.SetMarkerStyle(22)

mg = ROOT.TMultiGraph();
#mg.Add(data_graph)
mg.Add(mc_graph) # nominal mc
mg.Add(data_graph)
mg.Add(mc_0p3X0_10m_graph)
#mg.Add(mc_0p3X0_50m_graph) # experimental fit

mg.GetXaxis().SetTitle("E_{beam} [GeV]");
mg.GetYaxis().SetTitle("#sigma_{E}/<E>");

c_reso = ROOT.TCanvas("c_reso", "", 700, 600)
c_reso.cd()
gStyle.SetOptFit(0000)
gStyle.SetEndErrorSize(5.) # size of the brackets at the end of the error bars
#gPad.SetLogx()
#gPad.SetLogy()


#mg.Draw("apl")
#mg.Draw("apl")
mg.Draw("ap")

#mg.GetXaxis().SetLimits(-100,1000);
#leg =  ROOT.TLegend(0.45,0.6,0.6,0.90);
> leg =  ROOT.TLegend(0.5,0.4,0.8,0.90);


# Fit the resolution
fRes_data = TF1("res", "[0]/sqrt(x)+[1]",1,3500)
fRes_data.SetParName(0,"sqrt")
fRes_data.SetParName(1,"const")
fRes_data.SetLineColor(kBlack)
fitResult = data_graph.Fit(fRes_data, 'SQR')
sqrtTerm=fitResult.Get().Parameter(0)
sqrtTermError=fitResult.Get().Error(0)
constTerm=fitResult.Get().Parameter(1)
constTermError=fitResult.Get().Error(1)
print('data sqrtTerm = ',sqrtTerm, ' +/- ', sqrtTermError)
print('data constTerm = ',constTerm, '+/- ', constTermError)
fRes_sim = TF1("resSim", "[0]/sqrt(x)+[1]",1,3500)
fRes_sim.SetLineColor(kRed+1)
fitResult = mc_graph.Fit(fRes_sim, 'SQR')
sqrtTerm_caloOnly=fitResult.Get().Parameter(0)
sqrtTerm_caloOnlyError=fitResult.Get().Error(0)
constTerm_caloOnly=fitResult.Get().Parameter(1)
constTerm_caloOnlyError=fitResult.Get().Error(1)
print('simulation nominal')
print('sim sqrtTerm = ',sqrtTerm_caloOnly, ' +/- ', sqrtTerm_caloOnlyError)
print('sim constTerm = ',constTerm_caloOnly, '+/- ', constTerm_caloOnlyError)

fRes_simUpstream = TF1("resSimUpstream", "[0]/sqrt(x)+[1]",1,3500)
fRes_simUpstream.SetLineColor(kGreen+1)
fitResult = mc_0p3X0_10m_graph.Fit(fRes_simUpstream, 'SQR')
sqrtTerm_0p2X0_50m=fitResult.Get().Parameter(0)
sqrtTerm_0p2X0_50mError=fitResult.Get().Error(0)
constTerm_0p2X0_50m=fitResult.Get().Parameter(1)
constTerm_0p2X0_50mError=fitResult.Get().Error(1)
print('0p2X0 at 50m')
print('sim sqrtTerm = ',sqrtTerm_0p2X0_50m, ' +/- ', sqrtTerm_0p2X0_50mError)
print('sim constTerm = ',constTerm_0p2X0_50m, '+/- ', constTerm_0p2X0_50mError)

leg.SetFillStyle(0);
leg.SetFillColor(0);
leg.SetBorderSize(0);
leg.SetTextFont(42);
leg.SetTextSize(0.035);

leg.AddEntry(data_graph,"Data","pe")
leg.AddEntry(mc_graph,"simulation nominal","pe") # mc nominal
#leg.AddEntry(mc_0p3X0_10m_graph,"simulation 0.2X0 50m","pe")
leg.AddEntry(mc_0p3X0_10m_graph,"simulation H2 beamline","pe")
#leg.AddEntry(mc_0p3X0_50m_graph,"simulation 0.2X0 10m","pe")
leg.AddEntry(fRes_data,"#splitline{a = ("+str(round(sqrtTerm,2)*100)+" #pm "+str(round(sqrtTermError,2)*100)+")% #sqrt{GeV}}{c = ("+str(round(constTerm,3)*100)+" #pm "+str(round(constTermError,3)*100)+")%}","l")
leg.AddEntry(fRes_simUpstream,"#splitline{a = ("+str(round(sqrtTerm_0p2X0_50m,2)*100)+" #pm "+str(round(sqrtTerm_0p2X0_50mError,2)*100)+")% #sqrt{GeV}}{c = ("+str(round(constTerm_0p2X0_50m,3)*100)+" #pm "+str(round(constTerm_0p2X0_50mError,3)*100)+")%}","l")
leg.Draw("same")

ROOT.myText(       0.3,  0.25, 1, "#frac{#sigma_{E}}{<E>} = #frac{a}{#sqrt{E_{beam}}} #oplus c", 0.035)

gPad.Modified();
#mg.GetHistogram().GetXaxis().SetRangeUser(-10.,3500)
#mg.GetXaxis().SetLimits(0.,2.)


#mg.GetXaxis().SetLimits(1e-1,1500.)
#mg.GetYaxis().SetLimits(0.,0.5)
# to set axis ranges use this
mg.GetXaxis().SetLimits(10,180.)
#mg.GetXaxis().SetLimits(10,3500.)
#mg.GetXaxis().SetLimits(10,1500.)
mg.SetMinimum(0.05)
mg.SetMaximum(0.3)
#mg.SetMaximum(0.5)

#mg.SetMinimum(50)

#gPad.Modified()
#gPad.Update()



c_reso.SaveAs("resolution_combined.pdf")



# data/mc ratio
gRatio = TGraphErrors()
nPoints = mc_graph.GetN()

for xi in range(nPoints):
    tmpX, tmpY = ctypes.c_double(0.), ctypes.c_double(0.)
    tmpX_data, tmpY_data = ctypes.c_double(0.), ctypes.c_double(0.)
    data_graph.GetPoint(xi,tmpX_data,tmpY_data)
    mc_graph.GetPoint(xi,tmpX,tmpY)
    ratio = float(tmpY_data.value/tmpY.value)
    xValue = float(tmpX.value)
    gRatio.SetPoint(xi,xValue,ratio)


c_ratio = ROOT.TCanvas("c_ratio", "", 700, 600)
c_ratio.cd()
gRatio.Draw("ap")
c_ratio.SaveAs("ratio.pdf")

