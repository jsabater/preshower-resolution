import ROOT
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import math
from math import sqrt
import numpy as np
import os
from os.path import exists
from array import array
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/j/jsabater/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()
'''
def DoubleSidedCB2(x, mu, width, a1, p1, a2, p2)
{
   u   = (x-mu)/width
   A1  = TMath::Power(p1/TMath::Abs(a1),p1)*TMath::Exp(-a1*a1/2)
   A2  = TMath::Power(p2/TMath::Abs(a2),p2)*TMath::Exp(-a2*a2/2)
   B1  = p1/TMath::Abs(a1) - TMath::Abs(a1)
   B2  = p2/TMath::Abs(a2) - TMath::Abs(a2)
   
   result(1)
   if:      (u<-a1) result *= A1*TMath::Power(B1-u,-p1)
   elif (u<a2):  result *= TMath::Exp(-u*u/2)
   else:            result *= A2*TMath::Power(B2+u,-p2)
   return result
}

def DoubleSidedCB(x, par)
{
  return(par[0] * DoubleSidedCB2(x[0], par[1],par[2],par[3],par[4],par[5],par[6]))
}
'''
def prepare_graph(graph, name, title, colour = 9, markerStyle = 21, factor = 1):
   # graph settings
   graph.SetTitle(title)
   graph.SetName(name)
   graph.SetMarkerStyle(markerStyle)
   graph.SetMarkerSize(1.4)
   graph.SetMarkerColor(colour)
   graph.SetLineColor(colour)
   # set Y axis
   graph.GetYaxis().SetTitleSize(0.06)
   graph.GetYaxis().SetTitleOffset(1.1)
   graph.GetYaxis().SetLabelSize(0.045)
   graph.GetYaxis().SetNdivisions(504)
   # set X axis
   graph.GetXaxis().SetTitleSize(0.07)
   graph.GetXaxis().SetTitleOffset(1.)
   graph.GetXaxis().SetLabelSize(0.05)
   graph.GetYaxis().SetNdivisions(506)

def prepare_second_graph(secondary, main, name, title, factor = 2):
   # graph settings
   secondary.SetTitle(title)
   secondary.SetName(name)
   secondary.SetMarkerStyle(main.GetMarkerStyle())
   secondary.SetMarkerSize(main.GetMarkerSize())
   secondary.SetMarkerColor(main.GetMarkerColor())
   secondary.SetLineColor(main.GetLineColor())
   # set X axis
   secondary.GetXaxis().SetLabelSize(0)
   secondary.GetXaxis().SetTitleSize(0)
   secondary.GetXaxis().SetTickLength(main.GetXaxis().GetTickLength()*factor)
   # set Y axis
   main.GetYaxis().CenterTitle()
   secondary.GetYaxis().CenterTitle()
   secondary.GetYaxis().SetLabelSize(main.GetYaxis().GetLabelSize()*factor)
   secondary.GetYaxis().SetLabelOffset(main.GetYaxis().GetLabelOffset()/factor)
   secondary.GetYaxis().SetTitleSize(main.GetYaxis().GetTitleSize()*factor)
   secondary.GetYaxis().SetTitleOffset(main.GetYaxis().GetTitleOffset()/factor)
   secondary.GetYaxis().SetTickLength(main.GetYaxis().GetTickLength())
   secondary.GetYaxis().SetNdivisions(506)

gReso = TGraphErrors()
gMean = TGraphErrors()
gSigma = TGraphErrors()
gLinearity = TGraphErrors()



#f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/geant4/simplifiedLayout/500GeV_6layers_1000events.root","READ")

geometry="preshower_calo"
events=str(10000)
files = ["Faser-Physics-008500-00000.raw.root","Faser-Physics-008499-00000.raw.root","Faser-Physics-008498-00000.raw.root","Faser-Physics-008496-00000.raw.root","Faser-Physics-008497-00000.raw.root"]
#files = ["Faser-Physics-008500-00000.raw.root"]
#files = ["Faser-Physics-008499-00000.raw.root"]

for ifile, filename in enumerate(files):
   # take the energy of the photon from the file name
   #energy = int(filename.split('GeV')[0])
   #print('energy '+str(energy))
   if "8496" in filename:
      energy = 100
   if "8497" in filename:
      energy = 150
   if "8498" in filename:
      energy = 80
   if "8499" in filename:
      energy = 50
   if "8500" in filename:
      energy = 20
   

   myFile="/eos/project/f/faser-commissioning/TestBeamNtuples2022/"+filename

   # continue if the file does not exist
   file_exists = os.path.exists(myFile)
   if not file_exists:
      print (filename, ' does not exist, skipping')
      continue
   # open the file
   #f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/allpix/noNoiseCenteredASIC/3p5W/"+filename,"READ")
   #f = ROOT.TFile("/eos/user/j/jsabater/unige/FASER/allpix/noNoiseCenteredASIC/3p5W_3X0/"+filename,"READ")
   f = ROOT.TFile(myFile,"READ")
   
   eventTree = f.Get("nt")

   # Resolution (charge)
   c_reso = ROOT.TCanvas("c_reso", "", 700, 600)
   c_reso.cd()
   #gStyle.SetOptFit(1111)
   gStyle.SetOptFit(0000)
   #if energy < 250:
      #xmax = energy/1.3
   #xmax = energy*1.5

   if energy == 20:
      #xmin_1 = 0 # original fit
      #xmin_1 = 8 
      xmin_1 = 16
      xmax_1 = 16 # original fit
      #xmin_1 = 0
      xmin_2 = xmax_1
      #xmin_2 = 16
      xmax_2 = 50 # original fit
      #xmax_2 = 55
      xmin_3 = xmax_2
      xmax_3 = 70 #original fit
      #xmax_3 = 80
      #xmax_3 = xmax_2
   if energy == 50:
      #xmin_1 = 10 # original
      xmin_1 = 50
      xmax_1 = 60
      xmin_2 = xmax_1
      xmax_2 = 135
      xmin_3 = xmax_2
      xmax_3 = 170
   if energy == 80:
      xmin_1 = 10
      xmax_1 = 110
      xmin_2 = xmax_1
      xmax_2 = 210
      xmin_3 = xmax_2
      xmax_3 = 250
   if energy == 100:
      xmin_1 = 10
      xmax_1 = 140
      xmin_2 = xmax_1
      xmax_2 = 250
      xmin_3 = xmax_2
      xmax_3 = 300
   if energy == 150:
      xmin_1 = 30
      xmax_1 = 220
      xmin_2 = xmax_1
      xmax_2 = 380
      xmin_3 = xmax_2
      xmax_3 = 420
   #bins = 200
   bins = 300
   xmin = xmin_1
   xmax = xmax_3
   #h_reso = ROOT.TH1F("h_reso", "h_reso", bins, 0, xmax+100)
   h_reso = ROOT.TH1F("h_reso", "h_reso", bins, 0, xmax)
   #   eventTree.Draw("(Edep_Si+Edep_Sci)/"+str(energy)+" >> h_reso","")
   eventTree.Draw("charge0 >> h_reso","charge0 > 0")
   # crystal ball function
   preFitReso = TF1("leak", "crystalball",xmin,xmax)
   # gaussian 
   #preFitReso_gaus = TF1("gausFit", "gaus",0,xmax)
   gaus_xmin = xmin
   gaus_xmax = xmax
   par = array( 'd', 9*[0.] )
   g1 = TF1("g1","gaus",xmin_1,xmax_1);
   g2 = TF1("g2","gaus",xmin_2,xmax_2);
   g3 = TF1("g2","gaus",xmin_3,xmax_3);
   preFitReso_gaus = TF1("preGaus", "gaus(0)+gaus(3)+gaus(6)",gaus_xmin,gaus_xmax)
   preFitReso_gaus.SetLineColor( 2 )
   preFitReso_gaus.SetLineWidth( 1 )
   h_reso.Fit(g1,'R')
   h_reso.Fit(g2,'R')
   h_reso.Fit(g3,'R')
   par1 = g1.GetParameters()
   par2 = g2.GetParameters()
   par3 = g3.GetParameters()
   par[0], par[1], par[2] = par1[0], par1[1], par1[2]
   par[3], par[4], par[5] = par2[0], par2[1], par2[2]
   par[6], par[7], par[8] = par3[0], par3[1], par3[2]
   preFitReso_gaus.SetParameters( par )
   resultPre_gaus = h_reso.Fit(preFitReso_gaus, "SQR","")
   print("chi2/ndf = ",preFitReso_gaus.GetChisquare() / preFitReso_gaus.GetNDF())
   finalGaus_xmin = resultPre_gaus.Get().Parameter(1) - 2. * resultPre_gaus.Get().Parameter(2)
   finalGaus_xmax = resultPre_gaus.Get().Parameter(7) + 2. * resultPre_gaus.Get().Parameter(8)
   #result_gaus = h_reso.Fit(preFitReso_gaus, "SQR")
   result_gaus = h_reso.Fit(preFitReso_gaus, "SQRME") # improved fit


   mean = result_gaus.Get().Parameter(4)
   dmean = result_gaus.Get().Error(4)
   sigma = result_gaus.Get().Parameter(5)
   dsigma = result_gaus.Get().Error(5)
   print('mean ',mean)
   print('sigma ',sigma)
   resolution = sigma/mean
   resolutionErrorSigma = dsigma / mean
   resolutionErrorMean = dmean * sigma / ( mean ** 2)
   resolutionError = sqrt( resolutionErrorSigma ** 2 +  resolutionErrorMean ** 2 )
   
   #fitReso_gaus.SetLineColor(kAzure)
   #fitReso_gaus.Draw("same")
   preFitReso_gaus.Draw("same")

   print('starting 1st integral')

   gReso.SetPoint(ifile, energy, resolution)
   gReso.SetPointError(ifile, 0, resolutionError)
   #gReso.SetPointError(ifile, 0, 0)
   # Mean graph
   gMean.SetPoint(ifile, energy, mean)
   gMean.SetPointError(ifile, 0, dmean)
   # Sigma graph
   gSigma.SetPoint(ifile, energy, sigma)
   gSigma.SetPointError(ifile, 0, dsigma)

   c_reso.SaveAs("plots/h_reso_"+geometry+"_"+str(energy)+".pdf")
   c_reso.Close()

c_energyReso = ROOT.TCanvas("c_energyReso", "", 700, 600)
c_energyReso.cd()
gPad.SetLogx()
#gPad.SetLogy()
colour = kRed + 1
#colour = kAzure + 1
prepare_graph(gReso, "resolution", ";E_{beam} (GeV);#sigma_{E}/<E>", colour, 21)
#fRes = TF1("res", "sqrt([0]*[0] + pow([1]/sqrt(x),2))",5,600)
fRes = TF1("res", "[0]/sqrt(x)+[1]",0.1,3500)
#fRes = TF1("res", "[0]*sqrt(x)",0.1,3500)
fRes.SetParName(0,"const")
#fRes.SetParName(1,"sqrt")
fRes.SetLineColor(colour)
# fit 1/sqrt(x)
fitResult = gReso.Fit(fRes, 'SQRN')
print('Reso a',fitResult.Get().Parameter(0))
print('Reso b',fitResult.Get().Parameter(1))


# Draw graph and all labels
gReso.Draw("ape")


c_energyReso.SaveAs("plots/h_reso_"+geometry+".pdf")
c_energyReso.Close()



prepare_graph(gMean, "mean", ";E_{beam} (GeV);Mean", colour, 21)
prepare_graph(gSigma, "sigma", ";E_{beam} (GeV);Sigma", colour, 21)


plots = TFile("energy_resolution_plots_"+geometry+".root","RECREATE")
gReso.Write()
gMean.Write()
gSigma.Write()
gLinearity.Write()
plots.Write()
plots.Close()
